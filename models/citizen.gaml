/*MASTER*/

/* Mieszkancy sa losowani
 * Dzialaja atraktory
 */

/**
 *  citizen
 *  Author: Piotr Palka
 *  Description: 
 */
model citizen


global
{
	file shape_file_budynki <- file("../includes/building.shp");
	file shape_file_drogi <- file("../includes/drogi_polyline.shp");
	file shape_file_granice <- file("../includes/miasto_region.shp");
	file shape_file_hexy <- file("../includes/heksy_region.shp");
	file shape_file_kontrowersje <- file ("../includes/kontrowersje_region.shp");	// 10.04.2018 Marek
	const ludzik_shape type: string <- '../images/ludzik1.png';
	geometry shape <- envelope(shape_file_granice);
	bool rysujLudziki <- false;
	bool saveToCSV <- false;
	string outputFile <- "output.csv";
	bool _3Dcity <- true;
	float step <- 15 # mn;
	int current_hour update: (time / #hour) mod 24;
	int current_day update: (time / #days) mod 7;	// 05.04.2018 Marek
	float fanRelation <- 0.8; //< relacja fan-idol		// 10.04.2018 Marek
	float antifanRelation <- 1.3; //< relacja ANTYfan-idol	// 10.04.2018 Marek
	bool fanRelation_active <- false;
	bool antifanRelation_active <- false;
	
	bool linearRelation_active <- false;
	bool tgRelation_active <- false;	// 18.04.2018 Marek
	bool thRelation_active <- false;	// 18.04.2018 Marek
	bool relation_test_active <- true;	// 18.04.2018 Marek	#TEST
	
	int atraktor_id <- 1;	// 27.04.2018 Marek
	int atraktor_start <- 1;	// cycle 
	int atraktor_stop <- 1000; 	// cycle
	float procent_protestujacych <- 0.3;
	
	bool isChangeParamOnLocation <- true;
	float meanTrustDistribution <- 0.5;
	float commonMod <- 0.00001;
	float eduMod <- 0.00001;
	float happMod <- 0.00001;
	float wealthMod <- 0.00001;
	float ageMod <- 0.00001;
	float mod <- 0.00001;
	float onLocationParamChange <- 0.005;
	float zmianaZaufaniaWmiejscuZamieszkania <- 0.5;
	float zmianaZaufaniaWmiejscuPracy <- -0.2;
	float zmianaZaufaniaWrozrywka <- 0.1;
	float zmianaZaufaniaWmiejscuPublicznym <- -0.5;
	float zmianaZaufaniaWMordorHuta <- -0.15;
	float zmianaZaufaniaPlucaStareMiasto <- 0.15;
	float strongInfluence <- 5;
	float midInfluence <- 1;
	float weakInfluence <- 0.2;
	int min_work_start <- 6;
	int max_work_start <- 8;
	int min_work_end <- 16;
	int max_work_end <- 18;
	int min_rozrywka_start <- 18;
	int max_rozrywka_start <- 20;
	int min_rozrywka_end <- 21;
	int max_rozrywka_end <- 23;
	int min_urzad_start <- 10;
	int max_urzad_start <- 12;
	int min_urzad_end <- 13;
	int max_urzad_end <- 15;
	int min_lekarz_start <- 8;
	int max_lekarz_start <- 10;
	int min_lekarz_end <- 11;
	int max_lekarz_end <- 13;
	float is_rozrywka <- 0.5;
	float proc_robotnikow <- 0.5;
	float proc_blokersow <- 0.8;
	float probChangeParamOnLoc <- 0.1;
	float pojdzie_do_urzedu <- 0.25;
	float pojdzie_do_lekarza <- 0.15;
	int nb_strongLeaderPlus <- 0;
	int nb_strongLeaderMinus <- 0;
	float min_speed <- 5.0 # km / # h;
	float max_speed <- 30.0 # km / # h;
	graph the_graph;
	int nb_person <- 300;
	list<budynki> kultura;
	list<budynki> urzedy;
	list<heksy> hexy;
	list<kontrowersje> kontrowersje_lista;	// 10.04.2018 Marek
	kontrowersje kontrowersja_curr;	// kontrowersja dzialajaca w tym doswiadczeniu 
	
	int eksperyment  <- 0; // 0: bez ustawien, 1: 2007 r. Polska
	
	init
	{

	//seed <- 1.0;
		create heksy from: shape_file_hexy with: [nazwa::string(read("NAZWA"))];
		create kontrowersje from : shape_file_kontrowersje with: [ID::int(read("ID"))];	// 10.04.2018 Marek
		create budynki from: shape_file_budynki with: [type:: string(read("NATURE"))]
		{
			height <- 25 + rnd(225);
			if type = "domki"
			{
				color <- rgb(255, 208, 160);
				lcolor <- # black;
			} else if type = "blokowisko"
			{
				color <- rgb(255, 255, 145);
				lcolor <- # black;
			} else if type = "biurowiec"
			{
				color <- rgb(208, 208, 208);
				lcolor <- # black;
			} else if type = "fabryka"
			{
				color <- rgb(128, 128, 128);
				lcolor <- # black;
			} else if type = "zabytek"
			{
				color <- rgb(160, 120, 0);
				lcolor <- # black;
			} else if type = "urzad"
			{
				color <- rgb(129, 0, 129);
				lcolor <- # black;
			} else if type = "rzeka"
			{
				color <- rgb(96, 203, 255);
				lcolor <- rgb(96, 203, 255);
				height <- 1;
			} else if type = "park"
			{
				color <- rgb(0, 208, 0);
				lcolor <- rgb(0, 208, 0);
				height <- 1;
			} else if type = "szkola"
			{
				color <- rgb(255, 0, 255);
				lcolor <- # black;
			} else if type = "przychodnia"
			{
				color <- rgb(0, 0, 254);
				lcolor <- # black;
			} else if type = "bulwary"
			{
				color <- rgb(211, 255, 144);
				lcolor <- rgb(211, 255, 144);
				height <- 1;
			}

			if (!_3Dcity)
			{
				lcolor <- color;
				height <- 0;
			}

		}

		create drogi from: shape_file_drogi;
//		list<drogi> droga <- drogi;		// 10.04.2018 Marek
		the_graph <- as_edge_graph(drogi);
		
		list<budynki> mieszkania <- budynki where (each.type = "domki");
		list<budynki> bloki <- budynki where (each.type = "blokowisko");
		list<budynki> biura <- budynki where (each.type = "biurowiec" or each.type = "szkola");
		list<budynki> fabryki <- budynki where (each.type = "fabryka");
		list<budynki> kultura <- budynki where (each.type = "zabytek" or each.type = "park" or each.type = "bulwary");
		list<budynki> urzedy <- budynki where (each.type = "urzad");
		list<budynki> przychodnie <- budynki where (each.type = "przychodnia");
		list<heksy> hexy <- heksy;
		kontrowersje_lista <- kontrowersje; // 10.04.2018 Marek
		kontrowersja_curr <- kontrowersje_lista at (atraktor_id-1);
		float agernd <- rnd(10000)/100;
			
		eduMod <- commonMod;
	    happMod <- commonMod;
		wealthMod <- commonMod;
		ageMod <- commonMod;
		mod <- commonMod;
		
		create person number: nb_strongLeaderPlus
		{
			isStrongLeader <- true;
			speed <- min_speed + rnd(max_speed - min_speed);
			start_work <- min_work_start + rnd((max_work_start - min_work_start) * 60) / 60;
			end_work <- min_work_end + rnd((max_work_end - min_work_end) * 60) / 60;
			start_rozrywka <- flip(is_rozrywka) ? min_rozrywka_start + rnd((max_rozrywka_start - min_rozrywka_start) * 60) / 60 : -1;
			end_rozrywka <- min_rozrywka_end + rnd((max_rozrywka_end - min_rozrywka_end) * 60) / 60;
			start_urzad <- min_urzad_start + rnd((max_urzad_start - min_urzad_start) * 60) / 60;
			end_urzad <- min_urzad_end + rnd((max_urzad_end - min_urzad_end) * 60) / 60;
			start_lekarz <- min_lekarz_start + rnd((max_lekarz_start - min_lekarz_start) * 60) / 60;
			end_lekarz <- min_lekarz_end + rnd((max_lekarz_end - min_lekarz_end) * 60) / 60;
			mieszka <- flip(proc_blokersow) ? one_of(bloki) : one_of(mieszkania);
			pracuje <- flip(proc_robotnikow) ? one_of(fabryki) : one_of(biura);
			urzeduje <- one_of(urzedy);
			rozrywasie <- one_of(kultura);
			objective <- "w_domu";
			location <- any_location_in(mieszka);
			location <- { location.x, location.y, mieszka.height };
			mojHex <- heksy closest_to (location);
			trustToPeople <- 1.0;
			trustToInstitutios <- rnd(1000) / 1000;
			altruism <- rnd(1000) / 1000;
			eductaion <- rnd(1000) / 1000;
			happiness <- rnd(1000) / 1000;
			wealth <- rnd(1000) / 1000;
			identity <- rnd(1000) / 1000;
			age <- rnd(1000) / 1000;
		}

		create person number: nb_strongLeaderMinus
		{
			isStrongLeader <- true;
			speed <- min_speed + rnd(max_speed - min_speed);
			start_work <- min_work_start + rnd((max_work_start - min_work_start) * 60) / 60;
			end_work <- min_work_end + rnd((max_work_end - min_work_end) * 60) / 60;
			start_rozrywka <- flip(is_rozrywka) ? min_rozrywka_start + rnd((max_rozrywka_start - min_rozrywka_start) * 60) / 60 : -1;
			end_rozrywka <- min_rozrywka_end + rnd((max_rozrywka_end - min_rozrywka_end) * 60) / 60;
			start_urzad <- min_urzad_start + rnd((max_urzad_start - min_urzad_start) * 60) / 60;
			end_urzad <- min_urzad_end + rnd((max_urzad_end - min_urzad_end) * 60) / 60;
			start_lekarz <- min_lekarz_start + rnd((max_lekarz_start - min_lekarz_start) * 60) / 60;
			end_lekarz <- min_lekarz_end + rnd((max_lekarz_end - min_lekarz_end) * 60) / 60;
			mieszka <- flip(proc_blokersow) ? one_of(bloki) : one_of(mieszkania);
			pracuje <- flip(proc_robotnikow) ? one_of(fabryki) : one_of(biura);
			urzeduje <- one_of(urzedy);
			rozrywasie <- one_of(kultura);
			objective <- "w_domu";
			location <- any_location_in(mieszka);
			location <- { location.x, location.y, mieszka.height };
			mojHex <- heksy closest_to (location);
			trustToPeople <- 0.0;
			trustToInstitutios <- rnd(1000) / 1000;
			altruism <- rnd(1000) / 1000;
			eductaion <- rnd(1000) / 1000;
			happiness <- rnd(1000) / 1000;
			wealth <- rnd(1000) / 1000;
			identity <- rnd(1000) / 1000;
			age <- rnd(1000) / 1000;
		}

		create person number: (nb_person - nb_strongLeaderPlus - nb_strongLeaderMinus)
		{
			isStrongLeader <- false;
			speed <- min_speed + rnd(max_speed - min_speed);
			start_work <- min_work_start + rnd((max_work_start - min_work_start) * 60) / 60;
			end_work <- min_work_end + rnd((max_work_end - min_work_end) * 60) / 60;
			start_rozrywka <- flip(is_rozrywka) ? min_rozrywka_start + rnd((max_rozrywka_start - min_rozrywka_start) * 60) / 60 : -1;
			end_rozrywka <- min_rozrywka_end + rnd((max_rozrywka_end - min_rozrywka_end) * 60) / 60;
			start_urzad <- min_urzad_start + rnd((max_urzad_start - min_urzad_start) * 60) / 60;
			end_urzad <- min_urzad_end + rnd((max_urzad_end - min_urzad_end) * 60) / 60;
			start_lekarz <- min_lekarz_start + rnd((max_lekarz_start - min_lekarz_start) * 60) / 60;
			end_lekarz <- min_lekarz_end + rnd((max_lekarz_end - min_lekarz_end) * 60) / 60;
			mieszka <- flip(proc_blokersow) ? one_of(bloki) : one_of(mieszkania);
			pracuje <- flip(proc_robotnikow) ? one_of(fabryki) : one_of(biura);
			urzeduje <- one_of(urzedy);
			rozrywasie <- one_of(kultura);
			leczySie <- one_of(przychodnie);
			objective <- "w_domu";
			location <- any_location_in(mieszka);
			location <- { location.x, location.y, mieszka.height };
			mojHex <- heksy closest_to (location);
			trustToPeople <- gauss(meanTrustDistribution, 0.1);
			trustToInstitutios <- rnd(1000) / 1000;
			altruism <- rnd(1000) / 1000;
			eductaion <- rnd(1000) / 1000;
			happiness <- rnd(1000) / 1000;
			wealth <- rnd(1000) / 1000;
			identity <- rnd(1000) / 1000;
			
			
			if(atraktor_id = 1){	//Biala plama
				if(age <= 0.5 and wealth >= 0.5  
					and(mojHex.nazwa = "Sypialnia" or mojHex.nazwa = "Centrum" or mojHex.nazwa = "Mordor")
					){	// mlodzi i zamozni z Sypialni, Centrum i Mordoru.
					kontrowersje_podatnosc <- 1.0;
				}
			} else if (atraktor_id = 2){	//PKiN
				if(identity >= 0.6){	// mocno identyfikujacy sie z miastem
					kontrowersje_podatnosc <- 1; 
				}
			} else if (atraktor_id = 3){	//Wyburzenie Huty Leninca
				if(mieszka distance_to kontrowersja_curr <= 750 #m){
					kontrowersje_podatnosc <- 1.0;
				} else if (mieszka distance_to kontrowersja_curr <= 1500 #m){
					kontrowersje_podatnosc <- 0.5;
				}
			}
			
			
			if (eksperyment = 0) {
				age <- rnd(1000) / 1000;
			} else if (eksperyment = 1) {
				// zaufanie
				identity <- gauss(0.83, 0.16);
				trustToPeople <- gauss(0.105, 0.1);
				trustToInstitutios <- gauss(0.46, 0.1);
				wealth <- gauss(0.34, 0.3);
				
				// wiek: 2005
				if (agernd <= 3.98) { 
					age <- rnd(1000) / 1000 * 4 + 0;
				} else if (agernd <= 7.66) {
					age <- rnd(1000) / 1000 * 5 + 4;
				} else if (agernd <= 11.76) {
					age <- rnd(1000) / 1000 * 5 + 9;
				} else if (agernd <= 16.97) {
					age <- rnd(1000) / 1000 * 5 + 14;
				} else if (agernd <= 24.69) {
					age <- rnd(1000) / 1000 * 5 + 19;
				} else if (agernd <= 34.12) {
					age <- rnd(1000) / 1000 * 5 + 24;
				} else if (agernd <= 42.67) {
					age <- rnd(1000) / 1000 * 5 + 29;
				} else if (agernd <= 48.72) {
					age <- rnd(1000) / 1000 * 5 + 34;
				} else if (agernd <= 54.43) {
					age <- rnd(1000) / 1000 * 5 + 39;
				} else if (agernd <= 62.17) {
					age <- rnd(1000) / 1000 * 5 + 44;
				} else if (agernd <= 70.99) {
					age <- rnd(1000) / 1000 * 5 + 49;
				} else if (agernd <= 78.31) {
					age <- rnd(1000) / 1000 * 5 + 54;
				} else if (agernd <= 82.90) {
					age <- rnd(1000) / 1000 * 5 + 59;
				} else if (agernd <= 87.69) {
					age <- rnd(1000) / 1000 * 5 + 64;
				} else if (agernd <= 92.42) {
					age <- rnd(1000) / 1000 * 5 + 69;
				} else if (agernd <= 96.35) {
					age <- rnd(1000) / 1000 * 5 + 74;
				} else if (agernd <= 100.00) {
					age <- rnd(1000) / 1000 * 21 + 79;
				}	
			}
			// wiek: 2017
		}

	}

}

entities
{
	species heksy
	{
		string nazwa;
		aspect base
		{
			draw shape color: # transparent border: # black;
		}

	}

	species budynki
	{
		string type;
		rgb color <- # gray;
		rgb lcolor <- # gray;
		int height;
		aspect base
		{
			draw shape color: color border: lcolor depth: height;
		}

	}

	species drogi
	{
		rgb color <- rgb(255, 111, 64);
		aspect base
		{
			draw shape color: color;
		}

	}
	
	/* 10.04.2018 Marek */
	species kontrowersje
	{	
		int ID;
		rgb color <- # red;
		rgb lcolor <- # red;
		int height;
		aspect base
		{
			draw shape color: color border: lcolor depth: height;
		}	
	} 
	
	species	person skills: [moving]
	{
		bool isStrongLeader <- false;
		float speed <- (2 + rnd(3)) # km / # h;
		rgb color <- # white;
		budynki mieszka <- nil;
		budynki pracuje <- nil;
		budynki urzeduje <- nil;
		budynki rozrywasie <- nil;
		budynki leczySie <- nil;
		heksy mojHex <- nil;
		int start_work;
		int end_work;
		int start_rozrywka;
		int end_rozrywka;
		int start_urzad;
		int end_urzad;
		int start_lekarz;
		int end_lekarz;
		string objective;
		point the_target <- nil;
		bool isMarried;
		int numOfChildren;
		float age min: 0.0 max: 1.0;
		float trustToPeople min: 0.0 max: 1.0;
		float trustToInstitutios min: 0.0 max: 1.0;
		float altruism min: 0.0 max: 1.0;
		float eductaion min: 0.0 max: 1.0;
		float happiness min: 0.0 max: 1.0;
		float wealth min: 0.0 max: 1.0;
		float identity min: 0.0 max: 1.0;
		
		float kontrowersje_podatnosc;	// Marek 17.05.2018
		
		list<person> inPersonalDistance update: person at_distance 12 # m; /*120 #cm; /* czy da sie od ??? 45cm */
		list<person> inSocietyDistance update: person at_distance 36 # m; /* czy da sie od ??? 120cm */
		list<person> inSocialNetworklDistance; // update: person where(abs(age - each.age) <= 0.05 or abs(eductaion - each.eductaion) <= 0.05);
		budynki znajdujeSieW update: budynki closest_to (location);
		heksy znajdujeSieWHexie update: heksy closest_to (location);
		aspect base
		{
		//draw  file(ludzik_shape) size: 25 color: rgb((1.0 - trustToPeople) * 255, trustToPeople* 255, 0);
			if rysujLudziki
			{
				draw file(ludzik_shape) size: 40; //color: rgb((1.0 - trustToPeople) * 255, trustToPeople* 255, 0);
			} else
			{
				draw sphere(20) color: !isStrongLeader ? rgb((1.0 - trustToPeople) * 255, trustToPeople * 255, 0) : # blue;
			}

		}

		reflex initSocialDistance when: cycle = 1
		{
			inSocialNetworklDistance <- person where (abs(age - each.age) <= 0.05 or abs(eductaion - each.eductaion) <= 0.05);
		}

		/**
		 * zachowania zwiazane z przemieszczeniem sie
		 */
		/* Marek 05.04.2018: Zmiana w zaleznosci od dnia tygodnia. W weekend nie pracuje.*/		 
		reflex dom_praca when: pracuje != nil and current_hour = start_work and objective = "w_domu" and current_day < 5
		{
			objective <- "pracuje";
			the_target <- any_location_in(pracuje);
		}

		reflex praca_dom when: mieszka != nil and current_hour = end_work and objective = "pracuje"
		{
			objective <- "w_domu";
			the_target <- any_location_in(mieszka);
		}

		reflex praca_urzad when: urzeduje != nil and objective = "pracuje" and flip(pojdzie_do_urzedu) and current_hour = start_urzad
		{
			objective <- "w_urzedzie";
			the_target <- any_location_in(urzeduje);
		}

		reflex urzad_praca when: pracuje != nil and objective = "w_urzedzie" and current_hour = end_urzad
		{
			objective <- "pracuje";
			the_target <- any_location_in(pracuje);
		}

		reflex   when: leczySie != nil and objective = "pracuje" and flip(pojdzie_do_lekarza) and current_hour = start_lekarz
		{
			objective <- "w_przychodni";
			the_target <- any_location_in(leczySie);
		}

		reflex lekarz_praca when: pracuje != nil and objective = "w_przychodni" and current_hour = end_lekarz
		{
			objective <- "pracuje";
			the_target <- any_location_in(pracuje);
		}

		/* Marek 18.05.2018: Agent idzie do atraktora:
		 * Z domu, tylko w danich gdy aktywany jest aktraktor, z p-nstwem, w godzinach takich jak rozrywka
		 */
		reflex dom_kontrowersje when: objective="w_domu" and  cycle > atraktor_start and cycle < atraktor_stop 
										and flip(kontrowersje_podatnosc) and flip (procent_protestujacych)
										and ((current_day < 5 and current_hour = start_rozrywka) or (current_day > 4 and current_hour > 8))
		{
			objective <- "w_kontrowersji";
//			write "Debug:\nAtraktor_id = "+ atraktor_id;
//			write "Debug:\nKontrowersje lista: = "+ kontrowersje_lista; 
			write "Agent: "+ self + " IDZIE do atraktora " + kontrowersja_curr.name;
			the_target <- any_location_in(kontrowersja_curr);
		}
		
		/* Marek 18.05.2018  Agent wraca od atraktora
		 * W godzinach powrotu z rozrywki*/
		reflex kontrowersje_dom when: objective="w_kontrowersji" 
								and ((current_day < 5 and current_hour = end_rozrywka) or (current_day > 4 and current_hour > 22))
		{
			write "Agent: "+ self + " WRACA z atraktora.";
			objective <- "w_domu";
			the_target <- any_location_in(mieszka);
		}
		

		/* Marek 05.04.2018: Zmiana w zaleznosci od dnia tygodnia. W weekend rozrywa się od rana.*/
		reflex dom_rozrywka when: rozrywasie != nil and objective = "w_domu" and ((current_day < 5 and current_hour = start_rozrywka)
								  or (current_day > 4 and current_hour > 9))
		{
			objective <- "w_rozrywce";
			the_target <- any_location_in(rozrywasie);
		}

		/* Marek 05.04.2018: Zmiana w zaleznosci od dnia tygodnia. W weekendy wraca koło 21.*/
		reflex rozywka_dom when: mieszka != nil and objective = "w_rozrywce" and ((current_day < 5 and current_hour = end_rozrywka)
										  or (current_day > 4 and current_hour > 21))
		{
			objective <- "w_domu";
			the_target <- any_location_in(mieszka); 
		}

		/**
         * porusza sie
        */
		reflex move when: the_target != nil
		{
			path path_followed <- self goto [target::the_target, on::the_graph, return_path::true];
			list<geometry> segments <- path_followed.segments;
			loop line over: segments
			{
				float dist <- line.perimeter;
			}

			if the_target = location
			{
				the_target <- nil;
				location <- { location.x, location.y, znajdujeSieW.height };
			}

		}

		/* Interakcja zogniskowana */
		/**
		 * Jesli agent spotkal apostola, to ustawian jest mocna zmienna
		 * Jesli agent spotkal nie apostola, to wyrzucamy
		 */
		reflex focusedInteraction
		{
			ask one_of(inPersonalDistance)
			{ //losowanie wartosci z listy inPersonalDistance
				if (self.isStrongLeader)
				{ // tylko dla strong leaderow
					float distance <- charactersDistance(myself, self);
					float direction <- compareTrustToPeople(myself, self);
					if (fanRelation_active and distance <= fanRelation)
					{
						do modifyTrust(self, myself, 0.0, 0.0, 0.0, 0.0, direction * mod * strongInfluence);
					} else if (antifanRelation_active and distance >= antifanRelation)
					{
						do modifyTrust(self, myself, 0.0, 0.0, 0.0, 0.0, -direction * mod * strongInfluence);
					} else if (linearRelation_active)
					{
						do modifyTrustLinear(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);
						
					}
					/* Marek 18.04.2018 */
					 else if(tgRelation_active)
					{
						do modifyTrustTg(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);	
					
					} else if(thRelation_active){
						
						do modifyTrustTh(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);
					
					} else if(relation_test_active){	/* #TEST */
						
						do modifyTrustLinear(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * weakInfluence);						
						do modifyTrustTg(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);
						do modifyTrustTh(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);
					}
					

				}

			}

		}

		/*Interakcja symboliczna */
		/**
		  * mocno zmienia zaufanie - albo mocno w gore , albo mocno w dol
		  */
		reflex symbolicInteraction
		{
			float mul <- (flip(0.5) ? 1 : -1);
			ask one_of(inSocietyDistance)
			{
				float distance <- charactersDistance(myself, self);
				float direction <- compareTrustToPeople(myself, self);
				if (fanRelation_active and distance <= fanRelation)
				{
					do modifyTrust(self, myself, 0.0, 0.0, 0.0, 0.0, direction * mod * mul * strongInfluence);
				} else if (antifanRelation_active and distance >= antifanRelation)
				{
					do modifyTrust(self, myself, 0.0, 0.0, 0.0, 0.0, -direction * mod * mul * strongInfluence);
				} else if (linearRelation_active)
				{
					do modifyTrustLinear(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * mul * strongInfluence);
				}
				
				/* Marek 18.04.2018 */
				 else if(tgRelation_active)
				{
					do modifyTrustTg(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);	
				
				} else if(thRelation_active){
					
					do modifyTrustTh(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);
				
				} else if(relation_test_active){	/* #TEST */
					do modifyTrustLinear(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * weakInfluence);
					do modifyTrustTg(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);
					do modifyTrustTh(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);
				}
				
				

			}

		}

		/* interakcja w sieci spolecznosciowej */
		/**
		  * zaufanie sie mocna zmienia- niski poziom zaufania
		  * w swoim gronie - rosnie umiarkowanie
		  * ogolnie - maleje - mocno
		  * wolno zmienna
		  */
		reflex socialNetInteraction
		{
			ask one_of(inSocialNetworklDistance)
			{
				float distance <- charactersDistance(myself, self);
				float direction <- compareTrustToPeople(myself, self);
				if (fanRelation_active and distance <= fanRelation)
				{
					do modifyTrust(self, myself, 0.0, 0.0, 0.0, 0.0, direction * mod * midInfluence);
				} else if (antifanRelation_active and distance >= antifanRelation)
				{
					do modifyTrust(self, myself, 0.0, 0.0, 0.0, 0.0, -direction * mod * weakInfluence);
				} else if (linearRelation_active)
				{
					do modifyTrustLinear(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * weakInfluence);
				}
				/* Marek 18.04.2018 */
				 else if(tgRelation_active)
				{
					do modifyTrustTg(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);	
				
				} else if(thRelation_active){
					
					do modifyTrustTh(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);
					
				} else if(relation_test_active){	/* #TEST */
	
					do modifyTrustLinear(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * weakInfluence);
					do modifyTrustTg(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);
					do modifyTrustTh(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);
	
				}

			}

		}
		/**
		  * zmienia parametry w wyniku przebywania na danym terenie
		  */
		reflex changeTrustOnLocation
		{
			if (!self.isStrongLeader)
			{ //TODO
				if (isChangeParamOnLocation and flip(probChangeParamOnLoc))
				{
					if (znajdujeSieWHexie != nil and znajdujeSieW != nil)
					{
						if (znajdujeSieWHexie = mojHex or znajdujeSieW = mieszka)
						{
							trustToPeople <- trustToPeople + onLocationParamChange * zmianaZaufaniaWmiejscuZamieszkania;
						}

						if (znajdujeSieW = pracuje)
						{
							trustToPeople <- trustToPeople + onLocationParamChange * zmianaZaufaniaWmiejscuPracy;
						}

						if (znajdujeSieW.type = "zabytek" or znajdujeSieW.type = "park" or znajdujeSieW.type = "bulwary")
						{
							trustToPeople <- trustToPeople + onLocationParamChange * zmianaZaufaniaWrozrywka;
						}

						if (znajdujeSieW.type = "urzad" or znajdujeSieW.type = "przychodnia")
						{
							trustToPeople <- trustToPeople + onLocationParamChange * zmianaZaufaniaWmiejscuPublicznym;
						}

						if (znajdujeSieWHexie.nazwa = "Mordor" or znajdujeSieWHexie.nazwa = "HutaLenina")
						{
							trustToPeople <- trustToPeople + onLocationParamChange * zmianaZaufaniaWMordorHuta;
						}

						if (znajdujeSieWHexie.nazwa = "ZielonePluca" or znajdujeSieWHexie.nazwa = "StareMiasto")
						{
							trustToPeople <- trustToPeople + onLocationParamChange * zmianaZaufaniaPlucaStareMiasto;
						}

					}

				}

			}

		} 
		
//		/* Marek: Badanie rozkladu podobienstwa cech miedzy agentami. */
//		reflex distanceDistribution
//		{
//			if(cycle = 100){
//				if (self.name = 'person0')
//				{
//					list person_list <- list (species_of(self));
//					loop i from: 0 to: length(person_list)-1 {
//						ask person_list at i {
//							float distance <- charactersDistance(myself, self);
//							float cosinusSimilarity <- characterCosineSimilarity(myself, self);
//							save [myself, self, distance, cosinusSimilarity] to: "podobiensto.csv" type: csv;
//						}
//					}
//				}
//			}
//		}

		/* zapisz csv */
		reflex save when: saveToCSV
		{
			
		//save [self, self.trustToPeople, self.trustToInstitutios, self.altruism, self.eductaion, self.happiness, self.wealth, self.identity, self.age, self.location] to: "output.csv" type: csv;
			save [self, self.trustToPeople] to: outputFile type: csv;
			
		}

		
		/* Marek 13.04.2018 */
		action modifyTrustLinear (person me, person you, float eduMod, float happMod, float wealthMod, float ageMod, float mod)
		{
			if (!me.isStrongLeader)
			{
				float c <- eduMod * (1.0 - me.eductaion) + happMod * (1.0 - me.happiness) + wealthMod * (1.0 - me.wealth) + ageMod * (1.0 - me.age) + mod;
				float delta <- you.trustToPeople - me.trustToPeople;
				
				/*#TEST*/
				if(!relation_test_active){
					me.trustToPeople <- me.trustToPeople + c*delta;
				} else{
					save [delta] to: 'modifyTrustLin.csv' type: csv;
				}

			}

		}
		
		/* Marek 13.04.2018	 */
		 
		 /* Marek 18.04.2018 
		  * Zmiana zaufania zgodnie z funkcją tangens(x).
		  * Wersja stromsza niż liniowa */
		action modifyTrustTg(person me, person you, float eduMod, float happMod, float wealthMod, float ageMod, float mod)
		{
			if (!me.isStrongLeader)
			{
				float c <- eduMod * (1.0 - me.eductaion) + happMod * (1.0 - me.happiness) + wealthMod * (1.0 - me.wealth) + ageMod * (1.0 - me.age) + mod;
				float delta <- tan_rad((you.trustToPeople - me.trustToPeople));
				
				/*#TEST*/
				if(!relation_test_active){
					me.trustToPeople <- me.trustToPeople + c*delta;
				} else{					
					save [delta] to: 'modifyTrustTg.csv' type: csv;
				}
			}
		}
		
		 /* Marek 18.04.2018 
		  * Zmiana zaufania zgodnie z funkcją tangensHiperboliczny(x).
		  * Wersja bardziej płaska niż liniowa */
		action modifyTrustTh(person me, person you, float eduMod, float happMod, float wealthMod, float ageMod, float mod)
		{
			if (!me.isStrongLeader)
			{
				float c <- eduMod * (1.0 - me.eductaion) + happMod * (1.0 - me.happiness) + wealthMod * (1.0 - me.wealth) + ageMod * (1.0 - me.age) + mod;
				float delta <- th(you.trustToPeople - me.trustToPeople);
				
				/*#TEST*/
				if(!relation_test_active){
					me.trustToPeople <- me.trustToPeople + c*delta;
				} else{
					save [delta] to: 'modifyTrustTh.csv' type: csv;
				}
				
			}
		}
		 
		/* Marek 13.04.2018 
		 * to samo co modifyTrustLinear? 
		 */
		action modifyTrust (person me, person you, float eduMod, float happMod, float wealthMod, float ageMod, float mod)
		{
			if (!me.isStrongLeader)
			{
				float c <- eduMod * (1.0 - eductaion) + happMod * (1.0 - happiness) + wealthMod * (1.0 - wealth) + ageMod * (1.0 - age) + mod;
				me.trustToPeople <- me.trustToPeople + c * (you.trustToPeople - me.trustToPeople);
			}

		}

		float modIdentity (person P)
		{
			float ident <- P.identity;
			if (P.znajdujeSieWHexie != nil and P.znajdujeSieW != nil)
			{
				if (P.znajdujeSieWHexie = P.mojHex or P.znajdujeSieW = P.mieszka)
				{
					ident <- ident + zmianaZaufaniaWmiejscuZamieszkania;
				}

				if (P.znajdujeSieW = P.pracuje)
				{
					ident <- ident + zmianaZaufaniaWmiejscuPracy;
				}

				if (P.znajdujeSieW.type = "zabytek" or P.znajdujeSieW.type = "park" or P.znajdujeSieW.type = "bulwary")
				{
					ident <- ident + zmianaZaufaniaWrozrywka;
				}

				if (P.znajdujeSieW.type = "urzad" or P.znajdujeSieW.type = "przychodnia")
				{
					ident <- ident + zmianaZaufaniaWmiejscuPublicznym;
				}

				if (P.znajdujeSieWHexie.nazwa = "Mordor" or P.znajdujeSieWHexie.nazwa = "HutaLenina")
				{
					ident <- ident + zmianaZaufaniaWMordorHuta;
				}

				if (P.znajdujeSieWHexie.nazwa = "ZielonePluca" or P.znajdujeSieWHexie.nazwa = "StareMiasto")
				{
					ident <- ident + zmianaZaufaniaPlucaStareMiasto;
				}

			}

			return ident;
		}

		/**
		 * odleglosc miedzy cechami dwoch agentow
		 * --------------------------------------
		 * d = sqrt(sum((v_i-w_i)^2))		- odleglosc
		 * cosAlfa = (v*w)/(||v||*||w||)	- kat
		 * --------------------------------------
		 * Wszystkie cechy opisujace agenta sa normalizowane do wartosci z przedzialu (0,1)
		 *  
		 */
		float charactersDistance (person myselfP, person selfP)
		{
			float distance <- vectorsDistance(myselfP, selfP);
			return distance;
		}

		float characterCosineSimilarity (person myselfP, person selfP)
		{
			float cosineSimilarity <- vectorsMuliply(myselfP, selfP) / (vectorEuclideanNorm(myselfP) * vectorEuclideanNorm(selfP));
			return cosineSimilarity;
		}

		/* Norma euklidesowa wektora
		* ||v|| = sqrt (sum ((v_i)^2))*/
		float vectorEuclideanNorm (person p)
		{
			float myIdent <- modIdentity(p);
			float accumulation <- p.trustToPeople ^ 2 + p.trustToInstitutios ^ 2 + p.altruism ^ 2 + p.happiness ^ 2 + (p.wealth) ^ 2 + myIdent ^ 2 + (p.eductaion) ^ 2;
			return sqrt(accumulation);
		}

		/*Oblczanie odleglosci pomiedzy dwoma wektorami*/
		float vectorsDistance (person p1, person p2)
		{
			float p1Ident <- modIdentity(p1);
			float p2Ident <- modIdentity(p2);
			float distance <- (p1.trustToPeople - p2.trustToPeople) ^ 2 + (p1.trustToInstitutios - p2.trustToInstitutios) ^ 2 + (p1.altruism - p2.altruism) ^
			2 + (p1.happiness - p2.happiness) ^ 2 + ((p1.wealth - p2.wealth)) ^ 2 + (p1Ident - p2Ident) ^ 2 + ((p1.eductaion - p2.eductaion)) ^ 2;
			if (p1.isMarried != p2.isMarried)
			{
				distance <- distance + 1;
			}

			return sqrt(distance);
		}

		/*Mnozenie dwoch wektorow*/
		float vectorsMuliply (person p1, person p2)
		{
			float p1Ident <- modIdentity(p1);
			float p2Ident <- modIdentity(p2);
			float
			result <- p1.trustToPeople * p2.trustToPeople + p1.trustToInstitutios * p2.trustToInstitutios + p1.altruism * p2.altruism + p1.happiness * p2.happiness + (p1.wealth * p2.wealth) + p1Ident * p2Ident + (p1.eductaion * p2.eductaion);
			if (p1.isMarried and p2.isMarried)
			{
				result <- result + 1;
			}

			return result;
		}

		/* Losowanie cechy, ktora ma zostac zmienion */
		int randomFeatures
		{
			int featureNum <- rnd(5); // zwraca wartosc z {0,...,5}
			return featureNum;
		}

		/*Funckja obliczajaca, czy wartosc cechy agneta mySelf jest wieksza od wartosci tej samej cechy agent yourSelf
		 * TRUE - wieksza
		 * FALSE - mniejsza
		 */
		bool myFeatureIsGreater (person mySelf, person yourSelf, int feature)
		{
			float myIdent <- modIdentity(mySelf);
			float yourIdent <- modIdentity(yourSelf);
			if (feature = 0)
			{
				if (mySelf.trustToPeople > yourSelf.trustToPeople)
				{
					return true;
				} else
				{
					return false;
				}

			} else if (feature = 1)
			{
				if (mySelf.trustToInstitutios > yourSelf.trustToInstitutios)
				{
					return true;
				} else
				{
					return false;
				}

			} else if (feature = 2)
			{
				if (mySelf.altruism > yourSelf.altruism)
				{
					return true;
				} else
				{
					return false;
				}

			} else if (feature = 3)
			{
				if (mySelf.happiness > yourSelf.happiness)
				{
					return true;
				} else
				{
					return false;
				}

			} else if (feature = 4)
			{
				if (mySelf.wealth > yourSelf.wealth)
				{
					return true;
				} else
				{
					return false;
				}

			} else if (feature = 5)
			{
				if (myIdent > yourIdent)
				{
					return true;
				} else
				{
					return false;
				}

			}

		}
		/** 
		 * Funckja obliczajaca, czy wartosc cechy agneta myself jest wieksz of wartosci tej samej cechy agent self
		 * +1 - wieksza
		 * -1 - mniejsza
		 */
		float compareTrustToPeople (person mySelf, person yourSelf)
		{
			return (mySelf.trustToPeople > yourSelf.trustToPeople) ? 1.0 : -1.0;
		}
		/** U podanego agenta P, modyfikujemy ceche FEATURE o wartosc VALUE 
		 * Wartość VALUE może być ujemna, wtedy wartosc cechy jest zmniejszana
		 * trustToPeople(0), trustToInstitutios(1), altruism(2), happiness(3), wealth(4), identity(5)
		 * */
		action modifyFeature (person p, int feature, float value)
		{
			if (feature = 0)
			{
				p.trustToPeople <- p.trustToPeople + value;
			} else if (feature = 1)
			{
				p.trustToInstitutios <- p.trustToInstitutios + value;
			} else if (feature = 2)
			{
				p.altruism <- p.altruism + value;
			} else if (feature = 3)
			{
				p.happiness <- p.happiness + value;
			} else if (feature = 4)
			{
				p.wealth <- p.wealth + value;
			} else if (feature = 5)
			{
				p.identity <- p.identity + value;
			}

		}
		/* Lista osob o podanym wieku */
		list<person> selectAge1 (int number)
		{
			list<person> personList <- where(person, age = number);
			return personList;
		}

		/* Lista osob w tym samym wieku co podana osoba */
		list<person> selectAge2 (person p)
		{
			list<person> personList <- where(person, age = p.age);
			return personList;
		}

		/* Lista osob w wieku z przedzialu (number-range, numer+range) */
		list<person> selectAge3 (int number, int range)
		{
			list<person> personList <- where(person, age >= number - range and age <= number + range);
			return personList;
		}
		
		/* Marek 18.04.2018 Tangens hiperboliczny*/
		float th (float x){
			float numerator <- exp(x) - exp(-x);
			float denominator <- exp(x) + exp(-x);
			return numerator/denominator;
			
		}

	}

}

experiment main_experiment type: gui until: (time > 1 # d)
{
	parameter "Liczba ludzi:" var: nb_person category: "People";
	parameter "Liczba apostolow (pozytywni):" var: nb_strongLeaderPlus category: "People";
	parameter "Liczba apostolow (negatywni):" var: nb_strongLeaderMinus category: "People";
	parameter "Prawdopodobienstwo ze sie rozerwie" var: is_rozrywka category: "People" min: 0.0 max: 1.0;
	parameter "Prawdopodobienstwo ze pojdzie do urzedu" var: pojdzie_do_urzedu category: "People" min: 0.0 max: 1.0;
	parameter "Prawdopodobienstwo ze pojdzie do lekarza" var: pojdzie_do_lekarza category: "People" min: 0.0 max: 1.0;
	parameter "Rysuj ludzi jako obrazki" var: rysujLudziki category: "Ustawienia";
	parameter "Zapisz plik CSV" var: saveToCSV category: "Ustawienia";
	parameter "Nazwa pliku: " var: outputFile category: "Ustanienia";
	parameter "Miasto 3D" var: _3Dcity category: "Ustawienia";
	parameter "Eksperyment 0: bez ustawien, 1: 2007 r. Polska" var: eksperyment category: "Model" among: [0, 1];
	parameter "Wartosc srednia rozkladu zaufania" var: meanTrustDistribution category: "Model";
	parameter "Prawdopodobienstwo zmiany parametru w lokalizacji" var: probChangeParamOnLoc category: "Model" min: 0.0 max: 1.0;
	parameter "Relacja fan" var: fanRelation_active category: "Relacje";
	parameter "Relacja antyfan" var: antifanRelation_active category: "Relacje";
	parameter "Relacja liniowa" var: linearRelation_active category: "Relacje";
	parameter "Relacja tangens" var: tgRelation_active category: "Relacje";	// 18.04.2018 Marek
	parameter "Relacja tangens hiperboliczny" var: thRelation_active category: "Relacje";	// 18.04.2018 Marek
	parameter "Test relacji" var: relation_test_active category: "Relacje";	// 18.04.2018 Marek
	parameter "Relacja miejsca" var: isChangeParamOnLocation category: "Relacje";
	
	parameter "ID atraktora" var: atraktor_id category: "Atraktor";	// 27.04.2018 Marek
	parameter "Cykl rozpoczęcia" var: atraktor_start category:"Atraktor";	// 27.04.2018 Marek
	parameter "Cykl zakończenia" var: atraktor_stop category:"Atraktor";	// 27.04.2018 Marek
	parameter "Procent osob ktore chodza na manifestacje" var: procent_protestujacych category: "Atraktor";	// 26.05.2018 Marek
	
	parameter "Zaufania" var: mod category: "Modyfikator szybkosci zmian";
	parameter "Zaufania wzgledem edukacji" var: eduMod category: "Modyfikator szybkosci zmian";
	parameter "Zaufania wzgledem szczesliwosci" var: happMod category: "Modyfikator szybkosci zmian";
	parameter "Zaufania wzgledem zasobnosci" var: wealthMod category: "Modyfikator szybkosci zmian";
	parameter "Zaufania wzgledem wieku" var: ageMod category: "Modyfikator szybkosci zmian";
	parameter "Zaufania od polozenia" var: onLocationParamChange category: "Modyfikator szybkosci zmian";
	parameter "W miejscu / dzielnicy zamieszkania" var: zmianaZaufaniaWmiejscuZamieszkania category: "Modyfikator zaufania";
	parameter "W miejscu pracy" var: zmianaZaufaniaWmiejscuPracy category: "Modyfikator zaufania";
	parameter "W miejscu rozrywki" var: zmianaZaufaniaWrozrywka category: "Modyfikator zaufania";
	parameter "W miejscu publicznym (przychodnia/urzad)" var: zmianaZaufaniaWmiejscuPublicznym category: "Modyfikator zaufania";
	parameter "W dzielnicach Huta Lenina / Mordor" var: zmianaZaufaniaWMordorHuta category: "Modyfikator zaufania";
	parameter "W dzielnicach Stare Miasto / Zielone Pluca" var: zmianaZaufaniaPlucaStareMiasto category: "Modyfikator zaufania";
	parameter "Najwczesniejsza godzina do pracy" var: min_work_start category: "Times" min: 6 max: 8;
	parameter "Najpozniejsza godzina do pracy" var: max_work_start category: "Times" min: 8 max: 10;
	parameter "Najwczesniejsza godzina z pracy" var: min_work_end category: "Times" min: 12 max: 14;
	parameter "Najpozniejsza godzina z pracy" var: max_work_end category: "Times" min: 15 max: 19;
	parameter "Najwczesniejsza godzina rozpoczecia rozrywki" var: min_rozrywka_start category: "Times" min: 20 max: 21;
	parameter "Najpozniejsza godzina rozpoczecia rozrywki" var: max_rozrywka_start category: "Times" min: 21 max: 22;
	parameter "Najwczesniejsza godzina zakonczenia rozrywki" var: min_rozrywka_end category: "Times" min: 22 max: 23;
	parameter "Najpozniejsza godzina zakonczenia rozrywki" var: max_rozrywka_end category: "Times" min: 23 max: 24;
	parameter "Najwczesniejsza godzina rozpoczecia urzedu" var: min_urzad_start category: "Times" min: 10 max: 11;
	parameter "Najpozniejsza godzina rozpoczecia urzedu" var: max_urzad_start category: "Times" min: 11 max: 12;
	parameter "Najwczesniejsza godzina zakonczenia urzedu" var: min_urzad_end category: "Times" min: 13 max: 14;
	parameter "Najpozniejsza godzina zakonczenia urzedu" var: max_urzad_end category: "Times" min: 14 max: 15;
	parameter "Najwczesniejsza godzina pojscia do lekarza" var: min_lekarz_start category: "Times" min: 10 max: 11;
	parameter "Najpozniejsza godzina pojscia do lekarza" var: max_lekarz_start category: "Times" min: 11 max: 12;
	parameter "Najwczesniejsza godzina pojscia do lekarza" var: min_lekarz_end category: "Times" min: 13 max: 14;
	parameter "Najpozniejsza godzina pojscia do lekarza" var: max_lekarz_end category: "Times" min: 14 max: 15;
	parameter "SHP dla mieszkan:" var: shape_file_budynki category: "GIS";
	parameter "SHP dla drog:" var: shape_file_drogi category: "GIS";
	parameter "SHP dla granic:" var: shape_file_granice category: "GIS";
	parameter "SHP dla hexow:" var: shape_file_hexy category: "GIS";
	parameter "SHP dla hexow:" var: shape_file_kontrowersje category: "GIS"; 	// 10.05.2018 Marek

	// TRY: Multi Symulation 
	//	init{
	//		create simulation with: [nb_person:: 20, nb_strongLeaderPlus:: 1, nb_strongLeaderMinus:: 1, outputFile:: 'output1.csv'];
	//		//create simulation with: [nb_strongLeaderPlus::2, nb_strongLeaderMinus::2, outputFile::'output2.csv'];
	//	}
	output
	{
		display miasto type: opengl ambient_light: 100
		{
			species heksy aspect: base;
			species budynki aspect: base;
			species drogi aspect: base;
			species person aspect: base;
			species kontrowersje aspect: base;	// 10.04.2018 Marek 
		}

		display chart_display refresh_every: 1
		{
			chart "People Trust" type: series size: { 1, 1.0 } position: { 0, 0 }
			{
			//data "Spia" value: person count (each.objective = "w_domu") color: #black ;
			//data "W pracy" value: person count (each.objective = "pracuje") color: #blue ;
			//data "W urzedzie" value: person count (each.objective = "w_urzedzie") color: #red ;
			//data "W rozrywce" value: person count (each.objective = "w_rozrywce") color: #green ;
				data "Trust to people" value: sum(person collect each.trustToPeople);
			}

		}

		monitor "Sum Identity" value: sum(person collect each.identity);
		monitor "Sum Trust to People" value: sum(person collect each.trustToPeople);
		file name: "results" type: text data: "" + time + "," + sum(person collect each.trustToPeople) refresh_every: 1;
	}
	
//	/* Marek 05.04.2018: Wydruk dnia tygodnia i godizny.*/
//		reflex printHourAndDayOfWeek{
//			write "Dzien: " + current_day + ". Godzina: " + current_hour +".";
//		}

}

experiment batch_experiment type: batch keep_seed: true until: (time > 1 # d)
{
	parameter "Liczba ludzi:" var: nb_person category: "People";
	parameter "Liczba apostolow (pozytywni):" var: nb_strongLeaderPlus category: "People";
	parameter "Liczba apostolow (negatywni):" var: nb_strongLeaderMinus category: "People";
	parameter "Eksperyment 0: bez ustawien, 1: 2007 r. Polska" var: eksperyment category: "Model";
	parameter "Wartosc srednia rozkladu zaufania" var: meanTrustDistribution category: "Model";
	parameter "Zaufania" var: mod category: "Modyfikator szybkosci zmian";
	parameter "Zaufania wzgledem edukacji" var: eduMod category: "Modyfikator szybkosci zmian";
	parameter "Zaufania wzgledem szczesliwosci" var: happMod category: "Modyfikator szybkosci zmian";
	parameter "Zaufania wzgledem zasobnosci" var: wealthMod category: "Modyfikator szybkosci zmian";
	parameter "Zaufania wzgledem wieku" var: ageMod category: "Modyfikator szybkosci zmian";
	parameter "Zaufania od polozenia" var: onLocationParamChange category: "Modyfikator szybkosci zmian";
	output
	{
		file name: "results" type: xml data: [time, nb_person];
		//file name: "results" type: text data: "" + time + "," + sum(person collect each.trustToPeople) refresh_every: 1;  
		display chart_display refresh_every: 1
		{
			chart "People Trust" type: series size: { 1, 1.0 } position: { 0, 0 }
			{
			//data "Spia" value: person count (each.objective = "w_domu") color: #black ;
			//data "W pracy" value: person count (each.objective = "pracuje") color: #blue ;
			//data "W urzedzie" value: person count (each.objective = "w_urzedzie") color: #red ;
			//data "W rozrywce" value: person count (each.objective = "w_rozrywce") color: #green ;
				data "Trust to people" value: sum(person collect each.trustToPeople);
			}

		}

	}

}

experiment batch_experiment_marek type:batch keep_seed: true until: ( cycle = 3 ) {
	
	parameter "Eksperyment 0: bez ustawien, 1: 2007 r. Polska" var: eksperyment among: [1];
	parameter "Leader Plus" var: nb_strongLeaderPlus min:0 max:1 step:1;
	parameter "Leader minus" var: nb_strongLeaderMinus among: [0, 1];
	parameter "Trust Distribution" var: meanTrustDistribution among: [0.2, 0.5];
	bool saveToCSV <- true;
	string outputFile <- "jajko.csv";
	int nb_person <-10;
	
//	 reflex save
//		{
		//save [self, self.trustToPeople, self.trustToInstitutios, self.altruism, self.eductaion, self.happiness, self.wealth, self.identity, self.age, self.location] to: "output.csv" type: csv;
//			save [self, self] to: "test/Distribution_"+meanTrustDistribution +"_PL_"+nb_strongLeaderPlus+"NL_"+nb_strongLeaderMinus+".csv" type: csv;
//		}
	
//    action _step_ {
//        save type:"shp" to:"Distribution_"+".shp" with: [eductaion::"education",age::"age"];
//    }
    
//    reflex save when: saveToCSV
//		{
//		//save [self, self.trustToPeople, self.trustToInstitutios, self.altruism, self.eductaion, self.happiness, self.wealth, self.identity, self.age, self.location] to: "output.csv" type: csv;
//			save [self, self] to: "Distribution_"+meanTrustDistribution +"_PL_"+nb_strongLeaderPlus+"NL_"+nb_strongLeaderMinus+".csv" type: csv;
//		}
	
}