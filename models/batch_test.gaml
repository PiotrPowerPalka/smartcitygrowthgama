/**
 *  citizen
 *  Author: Piotr Pa�ka
 *  Description: 
 */
model citizen


global
{
	file shape_file_budynki <- file("../includes/building.shp");
	file shape_file_drogi <- file("../includes/drogi_polyline.shp");
	file shape_file_granice <- file("../includes/miasto_region.shp");
	file shape_file_hexy <- file("../includes/heksy_region.shp");
	const ludzik_shape type: string <- '../images/ludzik1.png';
	geometry shape <- envelope(shape_file_granice);
	bool rysujLudziki <- false;
	bool saveToCSV <- true;
	string outputFile <- "output.csv";
	bool _3Dcity <- true;
	float step <- 15 # mn;
	int current_hour update: (time / # hour) mod 24;
	float fanRelation <- 0.2; //< relacja fan-idol
	float antifanRelation <- 10; //< relacja ANTYfan-idol
	bool fanRelation_active <- false;
	bool antifanRelation_active <- false;
	bool linearRelation_active <- true;
	bool isChangeParamOnLocation <- true;
	float meanTrustDistribution <- 0.5;
	float eduMod <- 0.005;
	float happMod <- 0.005;
	float wealthMod <- 0.005;
	float ageMod <- 0.005;
	float mod <- 0.005;
	float onLocationParamChange <- 0.005;
	float zmianaZaufaniaWmiejscuZamieszkania <- 0.5;
	float zmianaZaufaniaWmiejscuPracy <- -0.2;
	float zmianaZaufaniaWrozrywka <- 0.1;
	float zmianaZaufaniaWmiejscuPublicznym <- -0.5;
	float zmianaZaufaniaWMordorHuta <- -0.15;
	float zmianaZaufaniaPlucaStareMiasto <- 0.15;
	float strongInfluence <- 5;
	float midInfluence <- 1;
	float weakInfluence <- 0.2;
	int min_work_start <- 6;
	int max_work_start <- 8;
	int min_work_end <- 16;
	int max_work_end <- 18;
	int min_rozrywka_start <- 18;
	int max_rozrywka_start <- 20;
	int min_rozrywka_end <- 21;
	int max_rozrywka_end <- 23;
	int min_urzad_start <- 10;
	int max_urzad_start <- 12;
	int min_urzad_end <- 13;
	int max_urzad_end <- 15;
	int min_lekarz_start <- 8;
	int max_lekarz_start <- 10;
	int min_lekarz_end <- 11;
	int max_lekarz_end <- 13;
	float is_rozrywka <- 0.5;
	float proc_robotnikow <- 0.5;
	float proc_blokersow <- 0.8;
	float probChangeParamOnLoc <- 0.1;
	float pojdzie_do_urzedu <- 0.25;
	float pojdzie_do_lekarza <- 0.15;
	int nb_strongLeaderPlus <- 1;
	int nb_strongLeaderMinus <- 0;
	float min_speed <- 5.0 # km / # h;
	float max_speed <- 30.0 # km / # h;
	graph the_graph;
	int nb_person <- 1000;
	list<budynki> kultura;
	list<budynki> urzedy;
	list<heksy> hexy;
	init
	{

	//seed <- 1.0;
		create heksy from: shape_file_hexy with: [nazwa::string(read("NAZWA"))];
		create budynki from: shape_file_budynki with: [type:: string(read("NATURE"))]
		{
			height <- 25 + rnd(225);
			if type = "domki"
			{
				color <- rgb(255, 208, 160);
				lcolor <- # black;
			} else if type = "blokowisko"
			{
				color <- rgb(255, 255, 145);
				lcolor <- # black;
			} else if type = "biurowiec"
			{
				color <- rgb(208, 208, 208);
				lcolor <- # black;
			} else if type = "fabryka"
			{
				color <- rgb(128, 128, 128);
				lcolor <- # black;
			} else if type = "zabytek"
			{
				color <- rgb(160, 120, 0);
				lcolor <- # black;
			} else if type = "urzad"
			{
				color <- rgb(129, 0, 129);
				lcolor <- # black;
			} else if type = "rzeka"
			{
				color <- rgb(96, 203, 255);
				lcolor <- rgb(96, 203, 255);
				height <- 1;
			} else if type = "park"
			{
				color <- rgb(0, 208, 0);
				lcolor <- rgb(0, 208, 0);
				height <- 1;
			} else if type = "szkola"
			{
				color <- rgb(255, 0, 255);
				lcolor <- # black;
			} else if type = "przychodnia"
			{
				color <- rgb(0, 0, 254);
				lcolor <- # black;
			} else if type = "bulwary"
			{
				color <- rgb(211, 255, 144);
				lcolor <- rgb(211, 255, 144);
				height <- 1;
			}

			if (!_3Dcity)
			{
				lcolor <- color;
				height <- 0;
			}

		}

		create drogi from: shape_file_drogi;
		the_graph <- as_edge_graph(drogi);
		list<budynki> mieszkania <- budynki where (each.type = "domki");
		list<budynki> bloki <- budynki where (each.type = "blokowisko");
		list<budynki> biura <- budynki where (each.type = "biurowiec" or each.type = "szkola");
		list<budynki> fabryki <- budynki where (each.type = "fabryka");
		list<budynki> kultura <- budynki where (each.type = "zabytek" or each.type = "park" or each.type = "bulwary");
		list<budynki> urzedy <- budynki where (each.type = "urzad");
		list<budynki> przychodnie <- budynki where (each.type = "przychodnia");
		list<heksy> hexy <- heksy;
		create person number: nb_strongLeaderPlus
		{
			isStrongLeader <- true;
			speed <- min_speed + rnd(max_speed - min_speed);
			start_work <- min_work_start + rnd((max_work_start - min_work_start) * 60) / 60;
			end_work <- min_work_end + rnd((max_work_end - min_work_end) * 60) / 60;
			start_rozrywka <- flip(is_rozrywka) ? min_rozrywka_start + rnd((max_rozrywka_start - min_rozrywka_start) * 60) / 60 : -1;
			end_rozrywka <- flip(is_rozrywka) ? min_rozrywka_end + rnd((max_rozrywka_end - min_rozrywka_end) * 60) / 60 : -1;
			start_urzad <- min_urzad_start + rnd((max_urzad_start - min_urzad_start) * 60) / 60;
			end_urzad <- min_urzad_end + rnd((max_urzad_end - min_urzad_end) * 60) / 60;
			start_lekarz <- min_lekarz_start + rnd((max_lekarz_start - min_lekarz_start) * 60) / 60;
			end_lekarz <- min_lekarz_end + rnd((max_lekarz_end - min_lekarz_end) * 60) / 60;
			mieszka <- flip(proc_blokersow) ? one_of(bloki) : one_of(mieszkania);
			pracuje <- flip(proc_robotnikow) ? one_of(fabryki) : one_of(biura);
			urzeduje <- one_of(urzedy);
			rozrywasie <- one_of(kultura);
			objective <- "w_domu";
			location <- any_location_in(mieszka);
			location <- { location.x, location.y, mieszka.height };
			mojHex <- heksy closest_to (location);
			trustToPeople <- 1.0;
			trustToInstitutios <- rnd(1000) / 1000;
			altruism <- rnd(1000) / 1000;
			eductaion <- rnd(1000) / 1000;
			happiness <- rnd(1000) / 1000;
			wealth <- rnd(1000) / 1000;
			identity <- rnd(1000) / 1000;
			age <- rnd(1000) / 1000;
		}

		create person number: nb_strongLeaderMinus
		{
			isStrongLeader <- true;
			speed <- min_speed + rnd(max_speed - min_speed);
			start_work <- min_work_start + rnd((max_work_start - min_work_start) * 60) / 60;
			end_work <- min_work_end + rnd((max_work_end - min_work_end) * 60) / 60;
			start_rozrywka <- flip(is_rozrywka) ? min_rozrywka_start + rnd((max_rozrywka_start - min_rozrywka_start) * 60) / 60 : -1;
			end_rozrywka <- flip(is_rozrywka) ? min_rozrywka_end + rnd((max_rozrywka_end - min_rozrywka_end) * 60) / 60 : -1;
			start_urzad <- min_urzad_start + rnd((max_urzad_start - min_urzad_start) * 60) / 60;
			end_urzad <- min_urzad_end + rnd((max_urzad_end - min_urzad_end) * 60) / 60;
			start_lekarz <- min_lekarz_start + rnd((max_lekarz_start - min_lekarz_start) * 60) / 60;
			end_lekarz <- min_lekarz_end + rnd((max_lekarz_end - min_lekarz_end) * 60) / 60;
			mieszka <- flip(proc_blokersow) ? one_of(bloki) : one_of(mieszkania);
			pracuje <- flip(proc_robotnikow) ? one_of(fabryki) : one_of(biura);
			urzeduje <- one_of(urzedy);
			rozrywasie <- one_of(kultura);
			objective <- "w_domu";
			location <- any_location_in(mieszka);
			location <- { location.x, location.y, mieszka.height };
			mojHex <- heksy closest_to (location);
			trustToPeople <- 0.0;
			trustToInstitutios <- rnd(1000) / 1000;
			altruism <- rnd(1000) / 1000;
			eductaion <- rnd(1000) / 1000;
			happiness <- rnd(1000) / 1000;
			wealth <- rnd(1000) / 1000;
			identity <- rnd(1000) / 1000;
			age <- rnd(1000) / 1000;
		}

		create person number: (nb_person - nb_strongLeaderPlus - nb_strongLeaderMinus)
		{
			isStrongLeader <- false;
			speed <- min_speed + rnd(max_speed - min_speed);
			start_work <- min_work_start + rnd((max_work_start - min_work_start) * 60) / 60;
			end_work <- min_work_end + rnd((max_work_end - min_work_end) * 60) / 60;
			start_rozrywka <- flip(is_rozrywka) ? min_rozrywka_start + rnd((max_rozrywka_start - min_rozrywka_start) * 60) / 60 : -1;
			end_rozrywka <- flip(is_rozrywka) ? min_rozrywka_end + rnd((max_rozrywka_end - min_rozrywka_end) * 60) / 60 : -1;
			start_urzad <- min_urzad_start + rnd((max_urzad_start - min_urzad_start) * 60) / 60;
			end_urzad <- min_urzad_end + rnd((max_urzad_end - min_urzad_end) * 60) / 60;
			start_lekarz <- min_lekarz_start + rnd((max_lekarz_start - min_lekarz_start) * 60) / 60;
			end_lekarz <- min_lekarz_end + rnd((max_lekarz_end - min_lekarz_end) * 60) / 60;
			mieszka <- flip(proc_blokersow) ? one_of(bloki) : one_of(mieszkania);
			pracuje <- flip(proc_robotnikow) ? one_of(fabryki) : one_of(biura);
			urzeduje <- one_of(urzedy);
			rozrywasie <- one_of(kultura);
			leczySie <- one_of(przychodnie);
			objective <- "w_domu";
			location <- any_location_in(mieszka);
			location <- { location.x, location.y, mieszka.height };
			mojHex <- heksy closest_to (location);
			trustToPeople <- gauss(meanTrustDistribution, 0.1);
			trustToInstitutios <- rnd(1000) / 1000;
			altruism <- rnd(1000) / 1000;
			eductaion <- rnd(1000) / 1000;
			happiness <- rnd(1000) / 1000;
			wealth <- rnd(1000) / 1000;
			identity <- rnd(1000) / 1000;
			age <- rnd(1000) / 1000;
		}

	}

}

entities
{
	species heksy
	{
		string nazwa;
		aspect base
		{
			draw shape color: # transparent border: # black;
		}

	}

	species budynki
	{
		string type;
		rgb color <- # gray;
		rgb lcolor <- # gray;
		int height;
		aspect base
		{
			draw shape color: color border: lcolor depth: height;
		}

	}

	species drogi
	{
		rgb color <- rgb(255, 111, 64);
		aspect base
		{
			draw shape color: color;
		}

	} species

	person skills: [moving]
	{
		bool isStrongLeader <- false;
		float speed <- (2 + rnd(3)) # km / # h;
		rgb color <- # white;
		budynki mieszka <- nil;
		budynki pracuje <- nil;
		budynki urzeduje <- nil;
		budynki rozrywasie <- nil;
		budynki leczySie <- nil;
		heksy mojHex <- nil;
		int start_work;
		int end_work;
		int start_rozrywka;
		int end_rozrywka;
		int start_urzad;
		int end_urzad;
		int start_lekarz;
		int end_lekarz;
		string objective;
		point the_target <- nil;
		bool isMarried;
		int numOfChildren;
		float age min: 0.0 max: 1.0;
		float trustToPeople min: 0.0 max: 1.0;
		float trustToInstitutios min: 0.0 max: 1.0;
		float altruism min: 0.0 max: 1.0;
		float eductaion min: 0.0 max: 1.0;
		float happiness min: 0.0 max: 1.0;
		float wealth min: 0.0 max: 1.0;
		float identity min: 0.0 max: 1.0;
		list<person> inPersonalDistance update: person at_distance 12 # m; /*120 #cm; /* czy da sie od ??? 45cm */
		list<person> inSocietyDistance update: person at_distance 36 # m; /* czy da sie od ??? 120cm */
		list<person> inSocialNetworklDistance; // update: person where(abs(age - each.age) <= 0.05 or abs(eductaion - each.eductaion) <= 0.05);
		budynki znajdujeSieW update: budynki closest_to (location);
		heksy znajdujeSieWHexie update: heksy closest_to (location);
		aspect base
		{
		//draw  file(ludzik_shape) size: 25 color: rgb((1.0 - trustToPeople) * 255, trustToPeople* 255, 0);
			if rysujLudziki
			{
				draw file(ludzik_shape) size: 40; //color: rgb((1.0 - trustToPeople) * 255, trustToPeople* 255, 0);
			} else
			{
				draw sphere(20) color: !isStrongLeader ? rgb((1.0 - trustToPeople) * 255, trustToPeople * 255, 0) : # blue;
			}

		}

		reflex initSocialDistance when: cycle = 1
		{
			inSocialNetworklDistance <- person where (abs(age - each.age) <= 0.05 or abs(eductaion - each.eductaion) <= 0.05);
		}

		/**
		 * zachowania zwiazane z przemieszczeniem sie
		 */
		reflex dom_praca when: pracuje != nil and current_hour = start_work and objective = "w_domu"
		{
			objective <- "pracuje";
			the_target <- any_location_in(pracuje);
		}

		reflex praca_dom when: mieszka != nil and current_hour = end_work and objective = "pracuje"
		{
			objective <- "w_domu";
			the_target <- any_location_in(mieszka);
		}

		reflex praca_urzad when: urzeduje != nil and objective = "pracuje" and flip(pojdzie_do_urzedu) and current_hour = start_urzad
		{
			objective <- "w_urzedzie";
			the_target <- any_location_in(urzeduje);
		}

		reflex urzad_praca when: pracuje != nil and objective = "w_urzedzie" and current_hour = end_urzad
		{
			objective <- "pracuje";
			the_target <- any_location_in(pracuje);
		}

		reflex praca_lekarz when: leczySie != nil and objective = "pracuje" and flip(pojdzie_do_lekarza) and current_hour = start_lekarz
		{
			objective <- "w_przychodni";
			the_target <- any_location_in(leczySie);
		}

		reflex lekarz_praca when: pracuje != nil and objective = "w_przychodni" and current_hour = end_lekarz
		{
			objective <- "pracuje";
			the_target <- any_location_in(pracuje);
		}

		reflex dom_rozrywka when: rozrywasie != nil and objective = "w_domu" and start_rozrywka >= 0 and current_hour = start_rozrywka
		{
			objective <- "w_rozrywce";
			the_target <- any_location_in(rozrywasie);
		}

		reflex rozywka_dom when: mieszka != nil and objective = "w_rozrywce" and start_rozrywka >= 0 and current_hour = end_rozrywka
		{
			objective <- "w_domu";
			the_target <- any_location_in(mieszka);
		}

		/**
         * porusza sie
        */
		reflex move when: the_target != nil
		{
			path path_followed <- self goto [target::the_target, on::the_graph, return_path::true];
			list<geometry> segments <- path_followed.segments;
			loop line over: segments
			{
				float dist <- line.perimeter;
			}

			if the_target = location
			{
				the_target <- nil;
				location <- { location.x, location.y, znajdujeSieW.height };
			}

		}

		/* Interakcja zogniskowana */
		/**
		 * dla apostola mocnazmienna
		 * dla normalnych ludzi - wyrzucamy
		 */
		reflex focusedInteraction
		{
			ask one_of(inPersonalDistance)
			{ //losowanie wartosci z listy inPersonalDistance
				if (self.isStrongLeader)
				{ // tylko dla strong leaderow
					float distance <- charactersDistance(myself, self);
					float direction <- compareTrustToPeople(myself, self);
					if (fanRelation_active and distance <= fanRelation)
					{
						do modifyTrust(self, myself, 0.0, 0.0, 0.0, 0.0, direction * mod * strongInfluence);
					} else if (antifanRelation_active and distance >= antifanRelation)
					{
						do modifyTrust(self, myself, 0.0, 0.0, 0.0, 0.0, -direction * mod * strongInfluence);
					} else if (linearRelation_active)
					{
						do modifyTrustLinear(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * strongInfluence);
					}

				}

			}

		}

		/*Interakcja symboliczna */
		/**
		  * mocno zmienia zaufanie - albo mocno w gore , albo mocno w dol
		  */
		reflex symbolicInteraction
		{
			float mul <- (flip(0.5) ? 1 : -1);
			ask one_of(inSocietyDistance)
			{
				float distance <- charactersDistance(myself, self);
				float direction <- compareTrustToPeople(myself, self);
				if (fanRelation_active and distance <= fanRelation)
				{
					do modifyTrust(self, myself, 0.0, 0.0, 0.0, 0.0, direction * mod * mul * strongInfluence);
				} else if (antifanRelation_active and distance >= antifanRelation)
				{
					do modifyTrust(self, myself, 0.0, 0.0, 0.0, 0.0, -direction * mod * mul * strongInfluence);
				} else if (linearRelation_active)
				{
					do modifyTrustLinear(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * mul * strongInfluence);
				}

			}

		}

		/* interakcja w sieci spolecznosciowej */
		/**
		  * zaufanie sie mocna zmienia- niski poziom zaufania
		  * w swoim gronie - rosnie umiarkowanie
		  * ogolnie - maleje - mocno
		  * wolno zmienna
		  */
		reflex socialNetInteraction
		{
			ask one_of(inSocialNetworklDistance)
			{
				float distance <- charactersDistance(myself, self);
				float direction <- compareTrustToPeople(myself, self);
				if (fanRelation_active and distance <= fanRelation)
				{
					do modifyTrust(self, myself, 0.0, 0.0, 0.0, 0.0, direction * mod * midInfluence);
				} else if (antifanRelation_active and distance >= antifanRelation)
				{
					do modifyTrust(self, myself, 0.0, 0.0, 0.0, 0.0, -direction * mod * weakInfluence);
				} else if (linearRelation_active)
				{
					do modifyTrustLinear(self, myself, eduMod * strongInfluence, happMod * strongInfluence, wealthMod * strongInfluence, ageMod * strongInfluence, mod * weakInfluence);
				}

			}

		}
		/**
		  * zmienia parametry w wyniku przebywania na danym terenie
		  */
		reflex changeTrustOnLocation
		{
			if (!self.isStrongLeader)
			{ //TODO
				if (isChangeParamOnLocation and flip(probChangeParamOnLoc))
				{
					if (znajdujeSieWHexie != nil and znajdujeSieW != nil)
					{
						if (znajdujeSieWHexie = mojHex or znajdujeSieW = mieszka)
						{
							trustToPeople <- trustToPeople + onLocationParamChange * zmianaZaufaniaWmiejscuZamieszkania;
						}

						if (znajdujeSieW = pracuje)
						{
							trustToPeople <- trustToPeople + onLocationParamChange * zmianaZaufaniaWmiejscuPracy;
						}

						if (znajdujeSieW.type = "zabytek" or znajdujeSieW.type = "park" or znajdujeSieW.type = "bulwary")
						{
							trustToPeople <- trustToPeople + onLocationParamChange * zmianaZaufaniaWrozrywka;
						}

						if (znajdujeSieW.type = "urzad" or znajdujeSieW.type = "przychodnia")
						{
							trustToPeople <- trustToPeople + onLocationParamChange * zmianaZaufaniaWmiejscuPublicznym;
						}

						if (znajdujeSieWHexie.nazwa = "Mordor" or znajdujeSieWHexie.nazwa = "HutaLenina")
						{
							trustToPeople <- trustToPeople + onLocationParamChange * zmianaZaufaniaWMordorHuta;
						}

						if (znajdujeSieWHexie.nazwa = "ZielonePluca" or znajdujeSieWHexie.nazwa = "StareMiasto")
						{
							trustToPeople <- trustToPeople + onLocationParamChange * zmianaZaufaniaPlucaStareMiasto;
						}

					}

				}

			}

		} 
		

		/* zapisz csv */
		reflex save when: saveToCSV
		{
			if(cycle = 0){	//nowa nazwa pliku dla kazdej symulacji
				outputFile <- "DIS_"+meanTrustDistribution+"_L_"+nb_person+"_PL_"+nb_strongLeaderPlus+"_NL_"+nb_strongLeaderMinus;
//				write "New fiel: ";
//				write "DIS_" + meanTrustDistribution+"_L_"+nb_person+"_PL_"+nb_strongLeaderPlus+"_NL_"+nb_strongLeaderMinus;
			}
			save [self, self.trustToPeople] to: outputFile type: csv;
			
		}

		action modifyTrustLinear (person me, person you, float eduMod, float happMod, float wealthMod, float ageMod, float mod)
		{
			if (!me.isStrongLeader)
			{
				float c <- eduMod * (1.0 - eductaion) + happMod * (1.0 - happiness) + wealthMod * (1.0 - wealth) + ageMod * (1.0 - age) + mod;

				me.trustToPeople <- me.trustToPeople + c * (you.trustToPeople - me.trustToPeople);
			}

		}

		action modifyTrust (person me, person you, float eduMod, float happMod, float wealthMod, float ageMod, float mod)
		{
			if (!me.isStrongLeader)
			{
				float c <- eduMod * (1.0 - eductaion) + happMod * (1.0 - happiness) + wealthMod * (1.0 - wealth) + ageMod * (1.0 - age) + mod;
				me.trustToPeople <- me.trustToPeople + c * (you.trustToPeople - me.trustToPeople);
			}

		}

		float modIdentity (person P)
		{
			float ident <- P.identity;
			if (P.znajdujeSieWHexie != nil and P.znajdujeSieW != nil)
			{
				if (P.znajdujeSieWHexie = P.mojHex or P.znajdujeSieW = P.mieszka)
				{
					ident <- ident + zmianaZaufaniaWmiejscuZamieszkania;
				}

				if (P.znajdujeSieW = P.pracuje)
				{
					ident <- ident + zmianaZaufaniaWmiejscuPracy;
				}

				if (P.znajdujeSieW.type = "zabytek" or P.znajdujeSieW.type = "park" or P.znajdujeSieW.type = "bulwary")
				{
					ident <- ident + zmianaZaufaniaWrozrywka;
				}

				if (P.znajdujeSieW.type = "urzad" or P.znajdujeSieW.type = "przychodnia")
				{
					ident <- ident + zmianaZaufaniaWmiejscuPublicznym;
				}

				if (P.znajdujeSieWHexie.nazwa = "Mordor" or P.znajdujeSieWHexie.nazwa = "HutaLenina")
				{
					ident <- ident + zmianaZaufaniaWMordorHuta;
				}

				if (P.znajdujeSieWHexie.nazwa = "ZielonePluca" or P.znajdujeSieWHexie.nazwa = "StareMiasto")
				{
					ident <- ident + zmianaZaufaniaPlucaStareMiasto;
				}

			}

			return ident;
		}

		/**
		 * odleglosc miedzy cechami dwoch agentow
		 * --------------------------------------
		 * d = sqrt(sum((v_i-w_i)^2))		- odleglosc
		 * cosAlfa = (v*w)/(||v||*||w||)	- kat
		 * --------------------------------------
		 * Wszystkie cechy opisujace agenta sa normalizowane do wartosci z przedzialu (0,1)
		 *  
		 */
		float charactersDistance (person myselfP, person selfP)
		{
			float distance <- vectorsDistance(myselfP, selfP);
			return distance;
		}

		float characterCosineSimilarity (person myselfP, person selfP)
		{
			float cosineSimilarity <- vectorsMuliply(myselfP, selfP) / (vectorEuclideanNorm(myselfP) * vectorEuclideanNorm(selfP));
			return cosineSimilarity;
		}

		/* Norma euklidesowa wektora
		* ||v|| = sqrt (sum ((v_i)^2))*/
		float vectorEuclideanNorm (person p)
		{
			float myIdent <- modIdentity(p);
			float accumulation <- p.trustToPeople ^ 2 + p.trustToInstitutios ^ 2 + p.altruism ^ 2 + p.happiness ^ 2 + (p.wealth) ^ 2 + myIdent ^ 2 + (p.eductaion) ^ 2;
			return sqrt(accumulation);
		}

		/*Oblczanie odleglosci pomiedzy dwoma wektorami*/
		float vectorsDistance (person p1, person p2)
		{
			float p1Ident <- modIdentity(p1);
			float p2Ident <- modIdentity(p2);
			float distance <- (p1.trustToPeople - p2.trustToPeople) ^ 2 + (p1.trustToInstitutios - p2.trustToInstitutios) ^ 2 + (p1.altruism - p2.altruism) ^
			2 + (p1.happiness - p2.happiness) ^ 2 + ((p1.wealth - p2.wealth)) ^ 2 + (p1Ident - p2Ident) ^ 2 + ((p1.eductaion - p2.eductaion)) ^ 2;
			if (p1.isMarried != p2.isMarried)
			{
				distance <- distance + 1;
			}

			return sqrt(distance);
		}

		/*Mnozenie dwoch wektorow*/
		float vectorsMuliply (person p1, person p2)
		{
			float p1Ident <- modIdentity(p1);
			float p2Ident <- modIdentity(p2);
			float
			result <- p1.trustToPeople * p2.trustToPeople + p1.trustToInstitutios * p2.trustToInstitutios + p1.altruism * p2.altruism + p1.happiness * p2.happiness + (p1.wealth * p2.wealth) + p1Ident * p2Ident + (p1.eductaion * p2.eductaion);
			if (p1.isMarried and p2.isMarried)
			{
				result <- result + 1;
			}

			return result;
		}

		/* Losowanie cechy, ktora ma zostac zmienion */
		int randomFeatures
		{
			int featureNum <- rnd(5); // zwraca wartosc z {0,...,5}
			return featureNum;
		}

		/*Funckja obliczajaca, czy wartosc cechy agneta myself jest wieksz of wartosci tej samej cechy agent self
		 * TRUE - wieksza
		 * FALSE - mniejsza
		 */
		bool myFeatureIsGreater (person mySelf, person yourSelf, int feature)
		{
			float myIdent <- modIdentity(mySelf);
			float yourIdent <- modIdentity(yourSelf);
			if (feature = 0)
			{
				if (mySelf.trustToPeople > yourSelf.trustToPeople)
				{
					return true;
				} else
				{
					return false;
				}

			} else if (feature = 1)
			{
				if (mySelf.trustToInstitutios > yourSelf.trustToInstitutios)
				{
					return true;
				} else
				{
					return false;
				}

			} else if (feature = 2)
			{
				if (mySelf.altruism > yourSelf.altruism)
				{
					return true;
				} else
				{
					return false;
				}

			} else if (feature = 3)
			{
				if (mySelf.happiness > yourSelf.happiness)
				{
					return true;
				} else
				{
					return false;
				}

			} else if (feature = 4)
			{
				if (mySelf.wealth > yourSelf.wealth)
				{
					return true;
				} else
				{
					return false;
				}

			} else if (feature = 5)
			{
				if (myIdent > yourIdent)
				{
					return true;
				} else
				{
					return false;
				}

			}

		}
		/** 
		 * Funckja obliczajaca, czy wartosc cechy agneta myself jest wieksz of wartosci tej samej cechy agent self
		 * +1 - wieksza
		 * -1 - mniejsza
		 */
		float compareTrustToPeople (person mySelf, person yourSelf)
		{
			return (mySelf.trustToPeople > yourSelf.trustToPeople) ? 1.0 : -1.0;
		}
		/** U podanego agenta P, modyfikujemy ceche FEATURE o wartosc VALUE 
		 * Wartość VALUE może być ujemna, wtedy wartosc cechy jest zmniejszana
		 * trustToPeople(0), trustToInstitutios(1), altruism(2), happiness(3), wealth(4), identity(5)
		 * */
		action modifyFeature (person p, int feature, float value)
		{
			if (feature = 0)
			{
				p.trustToPeople <- p.trustToPeople + value;
			} else if (feature = 1)
			{
				p.trustToInstitutios <- p.trustToInstitutios + value;
			} else if (feature = 2)
			{
				p.altruism <- p.altruism + value;
			} else if (feature = 3)
			{
				p.happiness <- p.happiness + value;
			} else if (feature = 4)
			{
				p.wealth <- p.wealth + value;
			} else if (feature = 5)
			{
				p.identity <- p.identity + value;
			}

		}
		/* Lista osob o podanym wieku */
		list<person> selectAge1 (int number)
		{
			list<person> personList <- where(person, age = number);
			return personList;
		}

		/* Lista osob w tym samym wieku co podana osoba */
		list<person> selectAge2 (person p)
		{
			list<person> personList <- where(person, age = p.age);
			return personList;
		}

		/* Lista osob w wieku z przedzialu (number-range, numer+range) */
		list<person> selectAge3 (int number, int range)
		{
			list<person> personList <- where(person, age >= number - range and age <= number + range);
			return personList;
		}

	}

}

experiment main_experiment type:batch keep_seed: true until: ( cycle > 3 )
{
	/*----------------------------Mozna zmieniac-------------------------------------------------------------*/
	parameter "Liczba ludzi:" var: nb_person among: [10]; 
	parameter "Liczba apostolow (pozytywni):" var: nb_strongLeaderPlus category: "People" min:0 max:1 step:1;
	parameter "Liczba apostolow (negatywni):" var: nb_strongLeaderMinus category: "People" min:0 max:1 step:1;
	parameter "Trust Distribution" var: meanTrustDistribution among: [0.2, 0.5, 0.8];
	bool saveToCSV <- true;
	/*---------------------------------------------------------------------------------------------------- */
	
	float is_rozrywka <- 0.5;
	float pojdzie_do_urzedu <- 0.25;
	float pojdzie_do_lekarza <- 0.15;
	parameter "Rysuj ludzi jako obrazki" var: rysujLudziki category: "Ustawienia";
	parameter "Nazwa pliku: " var: outputFile category: "Ustanienia";
	parameter "Miasto 3D" var: _3Dcity category: "Ustawienia";
	parameter "Wartosc srednia rozkladu zaufania" var: meanTrustDistribution category: "Model";
	parameter "Prawdopodobienstwo zmiany parametru w lokalizacji" var: probChangeParamOnLoc category: "Model" min: 0.0 max: 1.0;
	parameter "Relacja fan" var: fanRelation_active category: "Relacje";
	parameter "Relacja antyfan" var: antifanRelation_active category: "Relacje";
	parameter "Relacja liniowa" var: linearRelation_active category: "Relacje";
	parameter "Relacja miejsca" var: isChangeParamOnLocation category: "Relacje";
	float eduMod <- 0.005;
	float happMod <- 0.005;
	float wealthMod <- 0.005;
	float ageMod <- 0.005;
	float mod <- 0.005;
	float onLocationParamChange <- 0.005;
	float zmianaZaufaniaWmiejscuZamieszkania <- 0.5;
	float zmianaZaufaniaWmiejscuPracy <- -0.2;
	float zmianaZaufaniaWrozrywka <- 0.1;
	float zmianaZaufaniaWmiejscuPublicznym <- -0.5;
	float zmianaZaufaniaWMordorHuta <- -0.15;
	float zmianaZaufaniaPlucaStareMiasto <- 0.15;
	int min_work_start <- 6;
	int max_work_start <- 8;
	int min_work_end <- 16;
	int max_work_end <- 18;
	int min_rozrywka_start <- 18;
	int max_rozrywka_start <- 20;
	int min_rozrywka_end <- 21;
	int max_rozrywka_end <- 23;
	int min_urzad_start <- 10;
	int max_urzad_start <- 12;
	int min_urzad_end <- 13;
	int max_urzad_end <- 15;
	int min_lekarz_start <- 8;
	int max_lekarz_start <- 10;
	int min_lekarz_end <- 11;
	int max_lekarz_end <- 13;

	output
	{
		display miasto type: opengl ambient_light: 100
		{
			species heksy aspect: base;
			species budynki aspect: base;
			species drogi aspect: base;
			species person aspect: base;
		}

		display chart_display refresh_every: 1
		{
			chart "People Trust" type: series size: { 1, 1.0 } position: { 0, 0 }
			{
			//data "Spia" value: person count (each.objective = "w_domu") color: #black ;
			//data "W pracy" value: person count (each.objective = "pracuje") color: #blue ;
			//data "W urzedzie" value: person count (each.objective = "w_urzedzie") color: #red ;
			//data "W rozrywce" value: person count (each.objective = "w_rozrywce") color: #green ;
				data "Trust to people" value: sum(person collect each.trustToPeople);
			}

		}

		monitor "Sum Identity" value: sum(person collect each.identity);
		monitor "Sum Trust to People" value: sum(person collect each.trustToPeople);
		file name: "results" type: text data: "" + time + "," + sum(person collect each.trustToPeople) refresh_every: 1;
	}



}